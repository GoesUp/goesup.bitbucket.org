
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ehrId = "";
  
  $.ajax({
    url: "https://rest.ehrscape.com/rest/v1/ehr",
    method: "POST",
    crossDomain: true,
    cache: false,
    async: false,
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", getAuthorization());
    },
    success: function (data) {
      ehrId = data.ehrId;
      var imena = [["Bojan", "MALE"],["Sonja", "FEMALE"],["Robert", "MALE"],["Tatjana", "FEMALE"],["Rok", "MALE"],["Katarina", "FEMALE"],["Boštjan", "MALE"],["Milena", "FEMALE"],["Stanislav", "MALE"],["Tanja", "FEMALE"],["Matjaž", "MALE"],["Sara", "FEMALE"],["Gregor", "MALE"],["Alenka", "FEMALE"],["Miha", "MALE"],["Tina", "FEMALE"],["Martin", "MALE"],["Ivana", "FEMALE"],["David", "MALE"],["Vesna", "FEMALE"],["Igor", "MALE"],["Martina", "FEMALE"],["Boris", "MALE"],["Majda", "FEMALE"],["Dejan", "MALE"],["Frančiška", "FEMALE"],["Dušan", "MALE"],["Urška", "FEMALE"],["Jan", "MALE"],["Nika", "FEMALE"],["Alojz", "MALE"],["Špela", "FEMALE"],["Žiga", "MALE"],["Terezija", "FEMALE"],["Nejc", "MALE"],["Tjaša", "FEMALE"],["Jure", "MALE"],["Helena", "FEMALE"],["Uroš", "MALE"],["Anica", "FEMALE"],["Blaž", "MALE"],["Dragica", "FEMALE"]];
      var priimki = ["Blatnik","Černe","Petrović","Gregorič","Mihelič","Lešnik","Zadravec","Hren","Leban","Lazar","Kodrič","Nemec","Mrak","Kosi","Hodžić","Debeljak","Ivančič","Žižek","Tavčar","Žibert","Jovanović","Miklavčič","Krivec","Jarc","Vovk","Marković","Vodopivec","Zver","Hribernik","Šinkovec","Blažič","Petrovič","Ramšak","Javornik","Jamnik","Popović"];
      var pickedName = Math.floor(Math.random()*imena.length);
      var userData = {
        firstNames: imena[pickedName][0],
        lastNames: priimki[Math.floor(Math.random()*priimki.length)],
        gender: imena[pickedName][1],
        dateOfBirth: Math.floor(Math.random() * (2000 - 1925) + 1925) + "-02-02",
        additionalInfo: {"ehrId": ehrId}
      };
      
      $.ajax({
        url: "https://rest.ehrscape.com/rest/v1/demographics/party",
        method: "POST",
        crossDomain: true,
        cache: false,
        async: false,
        data: JSON.stringify(userData),
        contentType: 'application/json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", getAuthorization());
        },
        success: function (newData) {
          var visina = 179;
          var teza = 70.00;
          var systolic = 95;
          var diastolic = 65;
          var utrip = 75;
          
          if (stPacienta == 2) {
            teza = 125.00;
            systolic = 143;
            diastolic = 91;
            utrip = 90;
          } else if (stPacienta == 3) {
            teza = 91.00;
            systolic = 125;
            diastolic = 83;
            utrip = 85;
          }
          
          var vitalSigns = {
    		    "ctx/language": "en",
    		    "ctx/territory": "SI",
    		    "ctx/time": new Date().toISOString(),
    		    "vital_signs/height_length/any_event/body_height_length": visina,
    		    "vital_signs/body_weight/any_event/body_weight": teza,
    		    "vital_signs/blood_pressure/any_event/systolic": systolic,
    		    "vital_signs/blood_pressure/any_event/diastolic": diastolic,
    		    "vital_signs/pulse:0/any_event:0/rate|magnitude": utrip,
    		    "vital_signs/pulse:0/any_event:0/rate|unit": "/min"
          };
          
          var params = {
    		    ehrId: ehrId,
    		    templateId: "Vital Signs",
    		    format: "FLAT",
    		    committer: "dietapp"
          };
          
          $.ajax({
            url: "https://rest.ehrscape.com/rest/v1/composition?" + $.param(params),
            method: "POST",
            crossDomain: true,
            cache: false,
            async: false,
            data: JSON.stringify(vitalSigns),
            contentType: 'application/json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", getAuthorization());
            },
            success: function(finalData) {
              console.log(finalData);
            },
            error: function(err)  {
              console.log(err);
            }
          });
        }
      });
      
    }
  });

  return ehrId;
}

$(document).ready(function() {
  if (
    !window.location.href.endsWith("index.html") &&
    !window.location.href.endsWith("index.html#") &&
    !window.location.href.endsWith("/") &&
    !window.location.href.endsWith("/#")
  ) {
    return;
  }
  
  var ehrData;
  var globalBMI;
  var maxCalories;
  var generated = 0;
  
  document.getElementById("pacientiDropdown").disabled = true;
  document.getElementById("step2Panel").style.visibility = "hidden";
  document.getElementById("step3Panel").style.visibility = "hidden";
  
  var getBasicDataFromEhr = function(ehrId) {
    var ehrDataObject = {};
    var error = false;
    
    $.ajax({
      url: "https://rest.ehrscape.com/rest/v1/demographics/ehr/" + ehrId + "/party",
      method: "GET",
      crossDomain: true,
      cache: false,
      async: false,
      beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", getAuthorization());
      },
      success: function (data) {
        var zdaj = new Date();
        var takrat = new Date(data.party.dateOfBirth);
        ehrDataObject.age = zdaj.getFullYear() - takrat.getFullYear();
        ehrDataObject.firstName = data.party.firstNames;
        ehrDataObject.lastName = data.party.lastNames;
        ehrDataObject.gender = data.party.gender;
        ehrDataObject.ehrId = ehrId;
      },
      error: function() {
        error = true;
      }
    });
    if (error) {
      return "error";
    }
    return ehrDataObject;
  };
  
  var getFullDataFromEhr = function(ehrId) {
    var ehrDataObject = getBasicDataFromEhr(ehrId);
    if (ehrDataObject == "error") {
      return "error";
    }
    var vitals = ["height", "weight", "blood_pressure", "pulse"];
    
    for (var i = 0; i < vitals.length; i++) {
      $.ajax({
        url: "https://rest.ehrscape.com/rest/v1/view/" + ehrId + "/"+vitals[i],
        method: "GET",
        crossDomain: true,
        cache: false,
        async: false,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", getAuthorization());
        },
        success: function (data) {
          
          switch (i) {
            case 0:
              if (data.length == 0) {
                ehrDataObject.height = 0;
              } else {
                ehrDataObject.height = data[0].height;
              }
              break;
            case 1:
              if (data.length == 0) {
                ehrDataObject.weight = 0;
              } else {
                ehrDataObject.weight = data[0].weight;
              }
              break;
            case 2:
              ehrDataObject.blood_pressure = {};
              
              if (data.length == 0) {
                ehrDataObject.blood_pressure.systolic = 0;
                ehrDataObject.blood_pressure.diastolic = 0;
              } else {
                ehrDataObject.blood_pressure.systolic = data[0].systolic;
                ehrDataObject.blood_pressure.diastolic = data[0].diastolic;
              }
              break;
            case 3:
              if (data.length == 0) {
                ehrDataObject.pulse = 0;
              } else {
                ehrDataObject.pulse = data[0].pulse;
              }
              break;
          }
        }
      });
    }
    ehrData = ehrDataObject;
    return ehrDataObject;
  };
  
  $("#generiranje").click(function() {
    var ehr1 = generirajPodatke(1);
    var ehr2 = generirajPodatke(2);
    var ehr3 = generirajPodatke(3);
    
    if (ehr1 == "" || ehr2 == "" || ehr3 == "") {
      // NAPAKA!!!!
    } else {
      // document.getElementById("prostorZaObvestiloOGeneriranju").innerHTML = '<span class="badge">Generirani pacienti: ' + ehr1 + ', ' + ehr2 + ', ' + ehr3 + '.</span>';
      var details1 = getBasicDataFromEhr(ehr1);
      var details2 = getBasicDataFromEhr(ehr2);
      var details3 = getBasicDataFromEhr(ehr3);
      
      document.getElementById("pacientiDropdown").disabled = false;
      document.getElementById("pacientiDropdown").innerHTML = `
        <option value="" disabled selected>Izberite generiran ehrId...</option>
        <option value="` + details1.ehrId + `">` + details1.firstName + " " + details1.lastName + " (" + details1.ehrId + `) - običajni vitalni podatki</option>
        <option value="` + details2.ehrId + `">` + details2.firstName + " " + details2.lastName + " (" + details2.ehrId + `) - prekomerna telesna teža</option>
        <option value="` + details3.ehrId + `">` + details3.firstName + " " + details3.lastName + " (" + details3.ehrId + `) - povišana telesna teža</option>
      `;
    }
  });
  
  var fillStepTwo = function(data) {
    var BMI = Math.round(data.weight / Math.pow((data.height / 100), 2) * 10) / 10;
    globalBMI = BMI;
    
    document.getElementById("step2Name").innerHTML = data.lastName + ", " + data.firstName;
    document.getElementById("step2Visina").innerHTML = data.height + " cm";
    document.getElementById("step2Teza").innerHTML = data.weight + " kg";
    document.getElementById("step2BMI").innerHTML = "BMI = " + BMI;
    document.getElementById("step2Tlak").innerHTML = data.blood_pressure.systolic + "/" + data.blood_pressure.diastolic + " mmHg";
    document.getElementById("step2Utrip").innerHTML = data.pulse + " na minuto";
    
    if (BMI >= 30) {
      document.getElementById("step2RowVisina").classList.add("danger");
      document.getElementById("step2RowTeza").classList.add("danger");
      document.getElementById("step2TezaStanje").innerHTML = "Huda debelost";
    } else if (BMI >= 25) {
      document.getElementById("step2RowVisina").classList.add("warning");
      document.getElementById("step2RowTeza").classList.add("warning");
      document.getElementById("step2TezaStanje").innerHTML = "Povišana telesna teža";
    } else if (BMI < 18.5) {
      document.getElementById("step2RowVisina").classList.add("warning");
      document.getElementById("step2RowTeza").classList.add("warning");
      document.getElementById("step2TezaStanje").innerHTML = "Podhranjenost";
    } else {
      document.getElementById("step2RowVisina").classList.add("success");
      document.getElementById("step2RowTeza").classList.add("success");
      document.getElementById("step2TezaStanje").innerHTML = "Normalna telesna teža";
    }
    
    if (data.blood_pressure.systolic >= 140 || data.blood_pressure.diastolic >= 90) {
      document.getElementById("step2RowTlak").classList.add("danger");
      document.getElementById("step2TlakStanje").innerHTML = "Hipertenzija (visok krvni tlak)";
    } else if (data.blood_pressure.systolic >= 120 || data.blood_pressure.diastolic >= 80) {
      document.getElementById("step2RowTlak").classList.add("warning");
      document.getElementById("step2TlakStanje").innerHTML = "Predhipertenzija";
    } else {
      document.getElementById("step2RowTlak").classList.add("success");
      document.getElementById("step2TlakStanje").innerHTML = "Normalen krvni tlak";
    }
    
    if (data.pulse >= 100) {
      document.getElementById("step2RowUtrip").classList.add("danger");
      document.getElementById("step2UtripStanje").innerHTML = "Visok utrip";
    } else if (data.pulse >= 80) {
      document.getElementById("step2RowUtrip").classList.add("warning");
      document.getElementById("step2UtripStanje").innerHTML = "Nekoliko visok utrip";
    } else {
      document.getElementById("step2RowUtrip").classList.add("success");
      document.getElementById("step2UtripStanje").innerHTML = "Nizek utrip";
    }
    
  };
  
  $("#korakEnaNaprej").click(function() {
    var manualEhrIdValue = document.getElementById("manualEhrId").value;
    var pacientiDropdown = document.getElementById("pacientiDropdown");
    
    var chosenEhrId = "";
    
    if (manualEhrIdValue != "") {
      chosenEhrId = manualEhrIdValue;
    } else {
      var chosenVal = pacientiDropdown.options[pacientiDropdown.selectedIndex].value;
      if (chosenVal == "" || chosenVal == "noData") {
        document.getElementById("step1Panel").classList.remove("panel-default");
        document.getElementById("step1Panel").classList.add("panel-danger");
        return;
      }
      generated = 1;
      chosenEhrId = pacientiDropdown.options[pacientiDropdown.selectedIndex].value;
    }
    
    var fullData = getFullDataFromEhr(chosenEhrId);
    
    if (fullData == "error") {
      document.getElementById("step1Panel").classList.remove("panel-default");
      document.getElementById("step1Panel").classList.add("panel-danger");
      return;
    }
    
    fillStepTwo(fullData);
    
    document.getElementById("step2Panel").style.visibility = "visible";
    document.getElementById("step1Panel").classList.remove("panel-default");
    document.getElementById("step1Panel").classList.remove("panel-danger");
    document.getElementById("step1Panel").classList.add("panel-success");
    document.getElementById("korakEnaNaprej").disabled = true;
    
  });
  
  var calculateCalories = function(age, weight, height, gender, activity) {
    var kalorije = 0;
    switch (gender) {
      case "FEMALE":
        kalorije = activity * (88.362 + (13.397 * weight) + (4.799 * height) - (5.677 * age));
        break;
      default:
        kalorije = activity * (88.362 + (13.397 * weight) + (4.799 * height) - (5.677 * age));
        break;
    }
    return Math.floor(kalorije);
  };
  
  var fillStepThree = function() {
    var cal = calculateCalories(ehrData.age, ehrData.weight, ehrData.height, ehrData.gender, 1.66);
    document.getElementById("amountKalorije").innerHTML = cal;
    maxCalories = cal;
    
    var suggestion = "ohranjanje";
    if (globalBMI >= 25) {
      suggestion = "zmanjšanje";
    } else if (globalBMI < 18.5) {
      suggestion = "povečanje";
    }
    
    document.getElementById("weightSuggetion").innerHTML = suggestion;
  };
  
  $("#radioAktivnost").click(function() {
    setTimeout(function() {
      var kalorije = 0;
      if (document.getElementById("aktivnostOption1").checked) {
        kalorije = calculateCalories(ehrData.age, ehrData.weight, ehrData.height, ehrData.gender, 1.45);
      } else if (document.getElementById("aktivnostOption2").checked) {
        kalorije = calculateCalories(ehrData.age, ehrData.weight, ehrData.height, ehrData.gender, 1.66);
      } else if (document.getElementById("aktivnostOption3").checked) {
        kalorije = calculateCalories(ehrData.age, ehrData.weight, ehrData.height, ehrData.gender, 2.1);
      }
      document.getElementById("amountKalorije").innerHTML = kalorije;
      maxCalories = kalorije;
    }, 50);
  });
  
  $("#korakDvaNaprej").click(function() {
    fillStepThree();
    
    document.getElementById("step2Panel").classList.remove("panel-default");
    document.getElementById("step2Panel").classList.add("panel-success");
    document.getElementById("korakDvaNaprej").disabled = true;
    document.getElementById("step3Panel").style.visibility = "visible";
    
  });
  
  var vsaZivila = [];
  
  var dodajZivilo = function() {
    var novaZivila = [];
    
    var params = {
      app_id: "0c4e5d1b",
      app_key: "855dc62a62081eaed521f7018101f196",
      ingr: document.getElementById("ziviloText").value
    };
    $.get(
      "https://api.edamam.com/api/food-database/parser?" + $.param(params),
      function(data) {
        for (var i = 0; i < data.parsed.length; i++) {
          if ("quantity" in data.parsed[i]) {
            var zivilo = {};
            zivilo.naziv = data.parsed[i].food.label;
            zivilo.edamamId = data.parsed[i].food.foodId;
            zivilo.kolicina = {};
            zivilo.kolicina.vrednost = data.parsed[i].quantity;
            zivilo.kolicina.naziv = data.parsed[i].measure.label;
            zivilo.kolicina.edamamUri = data.parsed[i].measure.uri;
            novaZivila.push(zivilo);
          }
        }
    
      pridobiHranilneVrednosti(novaZivila);
      }
    );
    
    // posodobiTabele();
  };
  
  var pridobiHranilneVrednosti = function(zivila) {
    console.log(zivila);
    
    var params = {
      app_id: "0c4e5d1b",
      app_key: "855dc62a62081eaed521f7018101f196"
    };
    
    for (var i = 0; i < zivila.length; i++ ) {
      var payload = {
        "ingredients": [
          {
            "quantity": zivila[i].kolicina.vrednost,
            "measureURI": zivila[i].kolicina.edamamUri,
            "foodId": zivila[i].edamamId
          }
        ]
      };
      zivila[i].hranilnost = {};
      
      var f = function(zivilo) {
        $.ajax({
          url: "https://api.edamam.com/api/food-database/nutrients?" + $.param(params),
          method: "POST",
          crossDomain: true,
          cache: false,
          contentType: 'application/json',
          data: JSON.stringify(payload),
          success: function(data) {
            console.log(data);
            zivilo.hranilnost.kalorije = data.calories;
            if ("FAT" in data.totalNutrients) {
              zivilo.hranilnost.mascobe = data.totalNutrients.FAT.quantity;
            }
            if ("PROCNT" in data.totalNutrients) {
              zivilo.hranilnost.beljakovine = data.totalNutrients.PROCNT.quantity;
            }
            if ("SUGAR" in data.totalNutrients) {
              zivilo.hranilnost.sladkor = data.totalNutrients.SUGAR.quantity;
            }
            if ("CHOCDF" in data.totalNutrients) {
              zivilo.hranilnost.ogljikoviHidrati = data.totalNutrients.CHOCDF.quantity;
            }
            vsaZivila.push(zivilo);
            posodobiTabele();
          }
        
        });
      };
      f(zivila[i]);
    }
  };
  
  var posodobiTabele = function() {
    var tabela3 = [];
    var tabela2 = [];
    var tabela1 = [];
    var fatData = [0,0,0];
    
    console.log(vsaZivila);
    var totalCalories = 0;
    for (var i = 0; i < vsaZivila.length; i++) {
      console.log(vsaZivila[i]);
      if (vsaZivila[i].hranilnost.mascobe * 13 > vsaZivila[i].hranilnost.kalorije) {
        tabela3.push(vsaZivila[i]);
        fatData[0] += vsaZivila[i].hranilnost.kalorije;
      } else if (vsaZivila[i].hranilnost.mascobe * 8 > vsaZivila[i].hranilnost.kalorije) {
        tabela2.push(vsaZivila[i]);
        fatData[1] += vsaZivila[i].hranilnost.kalorije;
      } else {
        tabela1.push(vsaZivila[i]);
        fatData[2] += vsaZivila[i].hranilnost.kalorije;
      }
      totalCalories += vsaZivila[i].hranilnost.kalorije;
    }
    
    var vsebina3 = "";
    for (var i = 0; i < tabela3.length; i++) {
      vsebina3 += "<tr><td>" + tabela3[i].kolicina.vrednost + " × " + tabela3[i].kolicina.naziv + "</td><td>" + tabela3[i].naziv + "</td><td>Kalorije: " + tabela3[i].hranilnost.kalorije + "<br>Maščobe: " + Math.floor(tabela3[i].hranilnost.mascobe) + " g" + "<br>Beljakovine: " + Math.floor(tabela3[i].hranilnost.beljakovine) + " g" + "<br>Ogljikovi hidrati: " + Math.floor(tabela3[i].hranilnost.ogljikoviHidrati) + " g" + "<br>Sladkor: " + Math.floor(tabela3[i].hranilnost.sladkor) + " g" + "</td></tr>";
    }
    if (vsebina3 != "") {
      document.getElementById("step3L3Tabela").innerHTML = vsebina3;
    }
    
    var vsebina2 = "";
    for (var i = 0; i < tabela2.length; i++) {
      vsebina2 += "<tr><td>" + tabela2[i].kolicina.vrednost + " × " + tabela2[i].kolicina.naziv + "</td><td>" + tabela2[i].naziv + "</td><td>Kalorije: " + tabela2[i].hranilnost.kalorije + "<br>Maščobe: " + Math.floor(tabela2[i].hranilnost.mascobe) + " g" + "<br>Beljakovine: " + Math.floor(tabela2[i].hranilnost.beljakovine) + " g" + "<br>Ogljikovi hidrati: " + Math.floor(tabela2[i].hranilnost.ogljikoviHidrati) + " g" + "<br>Sladkor: " + Math.floor(tabela2[i].hranilnost.sladkor) + " g" + "</td></tr>";
    }
    if (vsebina2 != "") {
      document.getElementById("step3L2Tabela").innerHTML = vsebina2;
    }
    
    var vsebina1 = "";
    for (var i = 0; i < tabela1.length; i++) {
      vsebina1 += "<tr><td>" + tabela1[i].kolicina.vrednost + " × " + tabela1[i].kolicina.naziv + "</td><td>" + tabela1[i].naziv + "</td><td>Kalorije: " + tabela1[i].hranilnost.kalorije + "<br>Maščobe: " + Math.floor(tabela1[i].hranilnost.mascobe) + " g" + "<br>Beljakovine: " + Math.floor(tabela1[i].hranilnost.beljakovine) + " g" + "<br>Ogljikovi hidrati: " + Math.floor(tabela1[i].hranilnost.ogljikoviHidrati) + " g" + "<br>Sladkor: " + Math.floor(tabela1[i].hranilnost.sladkor) + " g" + "</td></tr>";
    }
    if (vsebina1 != "") {
      document.getElementById("step3L1Tabela").innerHTML = vsebina1;
    }
    
    document.getElementById("step3L3").innerHTML = tabela3.length;
    document.getElementById("step3L2").innerHTML = tabela2.length;
    document.getElementById("step3L1").innerHTML = tabela1.length;
    
    document.getElementById("progressKalorije").style.width = 100 * totalCalories / maxCalories + "%";
    if (totalCalories > maxCalories) {
      document.getElementById("progressKalorije").classList.remove("progress-bar-success");
      document.getElementById("progressKalorije").classList.add("progress-bar-danger");
    }
    
    chartUpdate(fatData);
    
  };
  
  $("#dodajZivilo").click(dodajZivilo);
  
  
  var chartUpdate = function(fatData) {
    var ctx = document.getElementById('myChart');
    var myChart = new Chart(ctx, {
      type: 'doughnut',
      data: {
        labels: ['Zelo mastna hrana', 'Srednje mastna hrana', 'Manj mastna hrana'],
        datasets: [{
          label: '# of Votes',
          data: fatData,
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)'
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }
  
  
});




// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
// https://rapidapi.com/edamam/api/edamam-nutrition-analysis
// https://developer.edamam.com/food-database-api
// https://developer.edamam.com/food-database-api-docs
// http://bl.ocks.org/NPashaP/9994181